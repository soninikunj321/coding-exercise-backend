package com.example.demo.controller;

import com.example.demo.data.MenuRepository;
import com.example.demo.model.Menu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/menu")
public class NavigationMenuController {

    @Autowired
    private MenuRepository menuRepository;

    @GetMapping("/hello")
    public String home() {
        return "Spring is here! Hello World!";
    }

    @GetMapping("/list")
    public List<Menu> listAllItems(){
        List<Menu> list = (List<Menu>) menuRepository.findAll();
        System.out.println("List: " + list);
        List<Menu> parentList = list.stream()
                .filter(menu -> menu.getParent_id() == -1 )
                .collect(Collectors.toList());
        for(Menu menu : parentList){
            menu.setSubMenu(list.stream()
                    .filter(e -> e.getParent_id() == menu.getId() )
                    .collect(Collectors.toSet()));
        }

        return parentList;
    }

    @GetMapping("/list2")
    public List<Menu> listAllItems2(){
        List<Menu> list = (List<Menu>) menuRepository.findAll();
        System.out.println("List: " + list);
//        list.forEach(el->{
//            int parentId = el.getParent_id();
//            list.stream().map(e -> e.setSubMenu(
//
//            ));
//        });
        return list;
    }

    @PostMapping(path = "/addMenu", consumes = "application/json", produces = "application/json")
    public void addMenu(@RequestBody Menu menu){
        menuRepository.save(menu);
    }

    @DeleteMapping("/removeMenu/{id}")
    void deleteEmployee(@PathVariable Integer id) {
        menuRepository.deleteById(id);
    }
}
