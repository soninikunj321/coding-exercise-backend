package com.example.demo.data;

import com.example.demo.model.Menu;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "my_menu", path = "my_menu")
public interface MenuRepository extends CrudRepository<Menu, Integer> {

}
