package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "menu")
public class Menu implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "menu_id")
    private Integer id;

    @Column(name = "menu_name")
    private String name;

    @Column(name = "menu_url")
    @JsonProperty("linkUrl")
    private String url;

    @Column(name = "menu_parent_id")
    @JsonIgnore
    private Integer parent_id;

    @JsonManagedReference
    @OneToMany
    private Set<Menu> subMenu;

//    @ManyToOne
//    @JsonBackReference
//    private Menu parent;

    public Set<Menu> getSubMenu() {
        return subMenu;
    }

    public void setSubMenu(Set<Menu> subMenu) {
        this.subMenu = subMenu;
    }

//    public Menu getParent() {
//        return parent;
//    }
//
//    public void setParent(Menu parent) {
//        this.parent = parent;
//    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getParent_id() {
        return parent_id;
    }

    public void setParent_id(Integer parent_id) {
        this.parent_id = parent_id;
    }

    @Override
    public String toString() {
        return "Menu{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", url='" + url + '\'' +
//                ", parent_id=" + parent_id +
                ", subMenu=" + subMenu +
//                ", parent=" + parent +
                '}';
    }
}
