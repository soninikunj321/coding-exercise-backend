INSERT INTO demo_db.menu
(menu_id, menu_name, menu_url, menu_parent_id)
VALUES(1, 'Home', '/', -1);
INSERT INTO demo_db.menu
(menu_id, menu_name, menu_url, menu_parent_id)
VALUES(2, 'Technologies', '#', -1);
INSERT INTO demo_db.menu
(menu_id, menu_name, menu_url, menu_parent_id)
VALUES(3, 'Go Lang', '/go', 2);
INSERT INTO demo_db.menu
(menu_id, menu_name, menu_url, menu_parent_id)
VALUES(4, 'Java', '/java', 2);
INSERT INTO demo_db.menu
(menu_id, menu_name, menu_url, menu_parent_id)
VALUES(5, 'JavaScript', '#', 2);
INSERT INTO demo_db.menu
(menu_id, menu_name, menu_url, menu_parent_id)
VALUES(6, 'React', '/react', 5);
INSERT INTO demo_db.menu
(menu_id, menu_name, menu_url, menu_parent_id)
VALUES(7, 'Angular', '/angular', 5);
INSERT INTO demo_db.menu
(menu_id, menu_name, menu_url, menu_parent_id)
VALUES(8, 'Node', '/node', 5);
INSERT INTO demo_db.menu
(menu_id, menu_name, menu_url, menu_parent_id)
VALUES(9, 'About', '/about', -1);
