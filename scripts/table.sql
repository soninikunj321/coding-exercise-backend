CREATE TABLE demo_db.menu (
	menu_id INT auto_increment NOT NULL,
	menu_name varchar(100) NULL,
	menu_url varchar(100) NULL,
	menu_parent_id INT DEFAULT -1 NULL,
	CONSTRAINT menu_PK PRIMARY KEY (menu_id)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_0900_ai_ci;